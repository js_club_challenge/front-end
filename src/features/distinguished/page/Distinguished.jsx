import { useState, useEffect } from "react";

import RocketIcon  from "@mui/icons-material/Rocket";

import Aos from "aos";
import "aos/dist/aos.css";

import DistinCard from "../components/DistinCard";
import ProfileData from "../../../../data/Profiles.json";

const Distinguished = () => {
  const [showgotop, setShowgotop] = useState(false);
  useEffect(() => {
    const handleScroll = () => {
      if (window.scrollY > 300) {
        setShowgotop(true);
      } else {
        setShowgotop(false);
      }
    };
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);
  function toThetop() {
    document.body.scrollTop = 0; // iphone
    document.documentElement.scrollTop = 0;
  }
  // init thư viện aos
  useEffect(() => {
    Aos.init({ duration: 2000 });
  }, []);

  return (
    //css color background
    <div>
      <div
        style={{ display: "flex", justifyContent: "center" }}
        data-aos="flip-up"
      >
        <div style={{ background: "#bdb76b", paddingTop: "15px" }}>
          <h1
            style={{
              alignItems: "center",
              textAlign: "center",
              marginTop: "110px",
            }}
          >
            Bảng Vinh Danh Các Thành Viên Nổi Bật{" "}
          </h1>
          <div
            style={{
              display: "flex",
              flexWrap: "wrap",
              justifyContent: "space-evenly",
              marginTop: "50px",
              maxWidth: "950px",
            }}
          >
            {ProfileData.map((profile, id) => (
              <DistinCard key={ id } profile={ profile } />
            ))}
          </div>
        </div>
      </div>
      {showgotop && (
        <button
          style={{
            position: "fixed",
            right: 2,
            bottom: 20,
            color: "red",
            cursor: "pointer",
            backgroundColor: "transparent",
            border: "none",
          }}
          onClick={toThetop}
        >
          <RocketIcon sx={{ borderRadius: "10px", backgroundColor: "white" }} />
        </button>
      )}
    </div>
  );
}

export default Distinguished;