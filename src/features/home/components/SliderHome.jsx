import { useEffect } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import { Parallax, Pagination, Navigation } from "swiper";

import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import "./SliderHome.css";

import Aos from "aos";
import "aos/dist/aos.css";

export default function SliderHome() {
  useEffect(() => {
    Aos.init({ duration: 2000 });
  }, []);

  return (
    <div data-aos="faded-left">
      <Swiper
        style={{
          "--swiper-navigation-color": "#fff",
          "--swiper-pagination-color": "#fff",
        }}
        speed={600}
        parallax={true}
        pagination={{
          clickable: true,
        }}
        navigation={true}
        modules={[Parallax, Pagination, Navigation]}
        className="mySwiper"
      >
        <div
          slot="container-start"
          className="parallax-bg"
          style={{
            backgroundImage:
              "url( https://scontent.fhan4-3.fna.fbcdn.net/v/t1.6435-9/129369953_3557171791063978_6241510949494313950_n.jpg?_nc_cat=103&ccb=1-5&_nc_sid=730e14&_nc_ohc=bvMSMMG_ngkAX95WTin&_nc_ht=scontent.fhan4-3.fna&oh=00_AT_cohRDVNlJR17tFxKRi-Eoo-dddXDxuulBGz96FPyO0w&oe=6251D793)",
          }}
          // data-swiper-parallax="-23%"
        ></div>

        <SwiperSlide>
          <div className="Htitle" data-swiper-parallax="-300">
            Slide 1
          </div>

          <div className="Htext" data-swiper-parallax="-100">
            <p style={{ marginRight: "50" }}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam
              dictum mattis velit, sit amet faucibus felis iaculis nec. Nulla
              laoreet justo vitae porttitor porttitor. Suspendisse in sem justo.
              Integer laoreet magna nec elit suscipit, ac laoreet nibh euismod.
              Aliquam hendrerit lorem at elit facilisis rutrum. Ut at
              ullamcorper velit. Nulla ligula nisi, imperdiet ut lacinia nec,
              tincidunt ut libero. Aenean feugiat non eros quis feugiat.
            </p>
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className="Htitle" data-swiper-parallax="-300">
            Slide 2
          </div>
          <div className="Hsubtitle" data-swiper-parallax="-200">
            Subtitle
          </div>
          <div className="Htext" data-swiper-parallax="-100">
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam
              dictum mattis velit, sit amet faucibus felis iaculis nec. Nulla
              laoreet justo vitae porttitor porttitor. Suspendisse in sem justo.
              Integer laoreet magna nec elit suscipit, ac laoreet nibh euismod.
              Aliquam hendrerit lorem at elit facilisis rutrum. Ut at
              ullamcorper velit. Nulla ligula nisi, imperdiet ut lacinia nec,
              tincidunt ut libero. Aenean feugiat non eros quis feugiat.
            </p>
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className="Htitle" data-swiper-parallax="-300">
            Slide 3
          </div>
          <div className="Hsubtitle" data-swiper-parallax="-200">
            Subtitle
          </div>
          <div className="Htext" data-swiper-parallax="-100">
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam
              dictum mattis velit, sit amet faucibus felis iaculis nec. Nulla
              laoreet justo vitae porttitor porttitor. Suspendisse in sem justo.
              Integer laoreet magna nec elit suscipit, ac laoreet nibh euismod.
              Aliquam hendrerit lorem at elit facilisis rutrum. Ut at
              ullamcorper velit. Nulla ligula nisi, imperdiet ut lacinia nec,
              tincidunt ut libero. Aenean feugiat non eros quis feugiat.
            </p>
          </div>
        </SwiperSlide>
      </Swiper>
    </div>
  );
}
