import { useState } from "react";
import { AppBar, Box, Toolbar, IconButton, Typography, Menu, Container, Button } from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";
import Logojs from "../../assets/images/JS_logo_250x250.webp";
import { Link, useNavigate } from "react-router-dom";

const pages = [
  { name: "Trang Chủ", id: 1, route: "/" },
  { name: "Ban Ngành ", id: 2, route: "/department" },
  { name: "Vinh Danh ", id: 3, route: "/distinguished" },
  { name: "Blog", id: 4, route: "/blog" },
];

const Header = () => {
  const [anchorElNav, setAnchorElNav] = useState(null);
  const navigate = useNavigate();

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  return (
    <AppBar position="fixed" sx={{ align: "center" }}>
      <Container maxWidth="xl" color="red">
        <Toolbar disableGutters color="red" align="center">
          <Typography
            variant="h4"
            noWrap
            component="div"
            color="#12c2e9 "
            sx={{ mr: 8, display: { xs: "none", md: "flex" } }}
            cursor=" pointer"
          >
            <img
              src={ Logojs }
              alt="logo"
              style={{ cursor: "pointer", width: "60px", height: "60px" }}
              onClick={() => navigate("/")}
            />
          </Typography>

          <Box sx={{ flexGrow: 1, display: { xs: "flex", md: "none" } }}>
            <IconButton
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              //trang chu
              onClick={handleOpenNavMenu}
              color="inherit"
            >
              <MenuIcon style={{ color: "black" }} />
            </IconButton>

            <Menu
              display="grid"
              id="menu-appbar"
              anchorEl={anchorElNav}
              anchorOrigin={{
                vertical: "bottom",
                horizontal: "left",
              }}
              keepMounted
              transformOrigin={{
                vertical: "top",
                horizontal: "left",
              }}
              open={Boolean(anchorElNav)}
              onClose={handleCloseNavMenu}
              sx={{
                display: { xs: "block", md: "none" },
              }}
            >
              {pages.map((page) => (
                <Button
                  key={page.id}
                  onClick={() => navigate(page.route) && handleCloseNavMenu}
                  sx={{ my: 2, color: "black", display: "block" }}
                >
                  <Typography textAlign="center">{page.name}</Typography>
                </Button>
              ))}
            </Menu>
          </Box>

          <Typography
            variant="h4"
            color="#12c2e9 "
            noWrap
            component="div"
            sx={{
              flexGrow: 1,
              display: { xs: "flex", md: "none" },
              marginRight: 5,
            }}
          >
            <img
              src={Logojs}
              alt="logo"
              style={{ cursor: "pointer", width: "60px", height: "60px" }}
              onClick={() => navigate("/")}
            />
          </Typography>
          
          <Box
            sx={{
              flexGrow: 1,
              display: {
                xs: "none",
                md: "flex",
              },
            }}
          >
            {pages.map((page) => (
              // <Button
              //   key={page.id}
              //   // trang chu can routeer
              //   href={page.route}
              //   onClick={handleCloseNavMenu}
              //   sx={{ my: 2, color: "black", display: "block" }}
              // >
              //   {page.name}
              // </Button>
                <Button
                  key={page.id}
                  onClick={() => navigate(page.route) && handleCloseNavMenu}
                  sx={{ my: 2, color: "black", display: "block" }}
                >
                  <Typography textAlign="center">{page.name}</Typography>
                </Button>
            ))}
          </Box>
        </Toolbar>
      </Container>
    </AppBar>
  );
};

export default Header;