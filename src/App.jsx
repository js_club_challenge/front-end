import "./App.css";

import { Routes, Route, Outlet } from "react-router-dom";
import Header from "./components/layouts/Header";
import FooterCentered from "./components/layouts/FooterCentered";
import Home from "./features/home";
import Department from "./features/department";
import Distinguished, { ProfileDistin } from "./features/distinguished";
import NotFoundPage from "./components/NotFoundPage";

export function App() {
  return (
    <>
      <Header />

      <div style={{ position: "relative", zIndex: 1, backgroundColor: "white", paddingBottom: 120 }}>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/department" element={<Department />} />
          
          <Route path="/distinguished" element={<Outlet />} >
            <Route index element={<Distinguished />} />
            <Route path=":profileId" element={<ProfileDistin />} />
          </Route>

          <Route path="/blog" element={<h1>blog</h1>} />
          <Route path="*" element={<NotFoundPage />} />
        </Routes>
      </div>

      <FooterCentered />
    </>
  );
}
